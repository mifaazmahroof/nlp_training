import spacy
from spacy.gold import GoldParse
from spacy.scorer import Scorer
import json

def evaluate(ner_model, examples):
    scorer = Scorer()
    for input_, annot in examples:
        doc_gold_text = ner_model.make_doc(input_)
        gold = GoldParse(doc_gold_text, entities=annot)
        pred_value = ner_model(input_)
        print(index, "of" , rowCount)
        print(gold.words)
        print(gold.ner)
        scorer.score(pred_value, gold)
    return scorer.scores

examples = []

jsfile = json.load(open(r"D:\NLP_CCCCC\Jsons\master_data_2019_01_04_10_52_31.data", "r", encoding='utf-8'))

rowCount = len(jsfile['rows'])
index = 0

for row in jsfile['rows']:


    if index >= 80000:
        break

    index += 1

    if index <= 0:
        continue

    mydict = {}

    lst = []

    lst.append((row['start'], row['end'], row['label']))
    mydict['rows'] = lst

    examples.append((row['line'], lst))

print(len(examples))
ner_model = spacy.load("nl_model") # for spaCy's pretrained use 'en_core_web_sm'
results = evaluate(ner_model, examples)
print(results)