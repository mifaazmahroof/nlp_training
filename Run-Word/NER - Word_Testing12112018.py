import docx
import os
import spacy

wordFile = r"D:\NLP_Training\Input\RN201836001.docx"
ttt = os.path.splitext(wordFile)[0]
ttr = os.path.basename(ttt)
n = 0
nlp = spacy.load(r'D:\Training\Installable\nl_model-0.0.0\nl_model\nl_model-0.0.0')
doc = docx.Document(wordFile)
for n in range(len(doc.paragraphs)):
    p = doc.paragraphs[n]
    snt_t = p.text
    docFile = nlp(snt_t)
    # print(tokenized)
    for word in docFile.ents:
        wText = word.text
        wLabel = word.label_

        if wText in p.text:
            inline = p.runs
            for i in range(len(inline)):
                if wText in inline[i].text:
                    text = inline[i].text.replace(wText, '<ano as="' + wLabel + '">' + wText + '</ano>')
                    inline[i].text = text

    n = n + 1

doc.save(r"D:\NLP_Training\Output/" + ttr + ".docx")
